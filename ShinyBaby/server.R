#
# This is the server logic of a Shiny web application. You can run the 
# application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

library(shiny)

# Define server logic required to draw a histogram
shinyServer(function(input, output) {
  
  # Download the SSA zip file
  SSA_URL = "http://www.ssa.gov/oact/babynames/names.zip"
  download.file(url=SSA_URL,destfile="names.zip", method='auto')

  
  # Extract the zip file
  ssa_zip = unzip("names.zip", exdir="nameFiles")
  
  
  # Create a data.frame to hold all of the records
  records = data.frame("Year" = as.numeric(c()),
                       "Name" = as.character(c()),
                       "Gender" = as.character(c()),
                       "Occurrences" = as.numeric(c()), stringsAsFactors = F)
  
  # Get all the text files, in order
  # (Skips over the readme PDF.)
  txt_files = list.files("nameFiles")[-1]
  
  for(fname in txt_files){
    year = as.numeric(substr(as.character(fname),start = 4,stop=7))
    print(year)
    
    dat <- read.table(file = paste("nameFiles/",fname,sep=""), header = F, sep = ',',quote = "",stringsAsFactors = F)
    
    records = rbind(records, cbind.data.frame("Year"=c(rep(year,length(dat[,1]))), dat)) 
  }
  
  colnames(records) <- c("Year", "Name", "Gender", "Count")
  
  maleNames <- unique(records[records$Gender == 'M' & records$Count >=2500,"Name"])
  
  femaleNames <- unique(records[records$Gender == 'F' & records$Count >=2500,"Name"])
  
  
  # Render the years to be chosen from, defaulting to the most recent year  
  output$yearUI <- renderUI(tagList(
    selectInput("yearChosen", "Pick a Year", choices = unique(records$Year), selected = max(records$Year))
  ))
  
  
  # Filter to the year chosen
  YOI <- reactive({dplyr::filter(records, Year == as.numeric(input$yearChosen))})

  #output$foo <- renderDataTable(YOI())
  
  # Male data 
  mDat <- reactive({mDat = YOI()[YOI()$Gender=='M',]})
  
  # Female data 
  fDat <- reactive({mDat = YOI()[YOI()$Gender=='F',]})
  
  
  output$pM <- renderPlot({
    mNOI = head(dplyr::arrange(mDat(), desc(Count))[,"Name"], n = input$nObs)
    dat = dplyr::arrange(records[records$Name %in% c(mNOI) & records$Gender == 'M', ], Name, Year)
    p <- ggplot2::ggplot(data = dat, ggplot2::aes(x=Year, y=Count, group = Name, color=Name)) +
    ggplot2::geom_point() +
    ggplot2::geom_path() + 
    ggplot2::ggtitle(paste("The Most Popular Boys Names of ", as.character(input$yearChosen, " Diplayed Since 1880", sep="")))
    return(p) })



  output$pF <- renderPlot({
    fNOI = head(dplyr::arrange(fDat(), desc(Count))[,"Name"],input$nObs)
    dat = dplyr::arrange(records[records$Name %in% c(fNOI) & records$Gender == 'F', ], Name,Year)
    p <- ggplot2::ggplot(data = dat, ggplot2::aes(x=Year, y=Count, group = Name, color=Name)) +
    ggplot2::geom_point() +
    ggplot2::geom_path() + 
    ggplot2::ggtitle(paste("The Most Popular Girle Names of ", as.character(input$yearChosen, " Diplayed Since 1880", sep="")))
    return(p) })

  
  output$tM <- renderDataTable({
    mDat()})


  output$tF <- renderDataTable({
    fDat()})
  
  
  
  
  observeEvent(input$mRefresh,
  
                output$mNames <- renderDataTable({data.frame( 
                  `First Name` = sample(x=c(maleNames), size = 10),
                 `Middle Name` = c(sample(maleNames,size = 10,replace = F)) )
                } ), ignoreNULL = F )
  
  
  
  observeEvent(input$fRefresh,
               
               output$fNames <- renderDataTable({data.frame( 
                 `First Name` = sample(x=c(femaleNames), size = 10),
                 `Middle Name` = c(sample(femaleNames,size = 10,replace = F)) )
               } ), ignoreNULL = F )
  
})
